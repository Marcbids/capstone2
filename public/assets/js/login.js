let login = document.querySelector("#logInUser")

login.addEventListener("submit", (e) => {
  	e.preventDefault()

  	let email = document.querySelector("#userEmail").value
  	let pw = document.querySelector("#password").value

  	if ((email == "") && (pw == "")){
  		alert("Enter credentials")
  	}else{

  		fetch('https://booking-system-mabid.herokuapp.com/api/user/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		//return a promise that resolves when res is loaded
		.then(res =>  res.json())//call this function when res is loaded
		.then(data =>{
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken);
				
				fetch('https://booking-system-mabid.herokuapp.com/api/user/details', {
					headers: {
					Authorization: `Bearer ${data.accessToken}`
					}	
				})
				.then(res => {
					return res.json()})
				.then(data => {
					
				//set the global user state to have properties containing authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("fullname", data.firstName + " " + data.lastName)
					localStorage.setItem("isAdmin", data.isAdmin)
					alert("Succesfully Logged in!")
					window.location.replace("./courses.html")
				})
			

			}else{
				alert("Incorrect credentials")
			}
			
		
		})
		}

})	

