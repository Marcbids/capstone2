let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
  	e.preventDefault()

  	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;
	let registerUser = document.querySelector("#registerUser").value;

	//validation to enable submit button when all fields are populated and both passwords match
	//mobile number is = 11

	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNumber.length === 11)){
		//check database
		//check for duplicate email in database first
		//url-where we can get the duplicate routes in our server
		//asynchronously load contents of the URL

		fetch('https://booking-system-mabid.herokuapp.com/api/user/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"email": userEmail
			})
		})
		//return a promise that resolves when res is loaded
		.then(res =>  res.json())//call this function when res is loaded
		.then(data =>{
			console.log(data)

/*==================================================================================*/
			//activity
			//ayusin yung bug
			//if true(duplicate email)return alert
			//else fetching
			if(data === false){
				//if no duplicates found
				//get the routes for registration in our server
				fetch('https://booking-system-mabid.herokuapp.com/api/user/', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						"firstName": firstName,
						"lastName": lastName,
						"email": userEmail,
						"password": password1,
						"mobileNo": mobileNumber
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					//if registration is successful
					if(data === true){
						alert("Registered Successfully")
						//redirect to login page
						window.location.replace("./login.html")
					}else{
						//error occured in registration
						alert("Something Went wrong")
					}

				})

			}else{
				alert("Email already exists")
			}
/*===================================================================================*/

			


		})
	}else{
		alert("Something Went Wrong, Please try again")
	}


});