let token = localStorage.getItem("token")
let id = localStorage.getItem("id")
let profile = document.querySelector("#profile")
let tables = document.querySelector("#tables")



fetch('https://booking-system-mabid.herokuapp.com/api/user/details',  {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {


	profile.innerHTML = 
	`
	<h2>Name: ${data.firstName} ${data.lastName} </h2>
	<h2>Email: ${data.email} </h2>
	<h2>Contact number: ${data.mobileNo} </h2>
	<table class="table" id="enrollments">
	  <thead>
	    <tr>
	      <th scope="col">Course ID</th>
	      <th scope="col">Enrolled on</th>
	      <th scope="col">Status</th>
	    </tr>
	  </thead>
	  <tbody id="tableBody">
	  </tbody>
	  </table>
	`

let tables = document.querySelector("#tableBody")
		
	userData = data.enrollments.map(result => {

		
			fetch(`https://booking-system-mabid.herokuapp.com/api/courses/${result.courseId}`)
			.then(res => res.json())
			.then(course => {
				console.log(result)
		})
		console.log(data)

				return(
					`	
					    <tr>
					      <th scope="row" id="courseName">${result.courseName}</th>
					      <td>${result.enrolledOn}</td>
					      <td>${result.status}</td>
					    </tr>
					
					`
				)
		
	}).join("")
tables.innerHTML = userData
})