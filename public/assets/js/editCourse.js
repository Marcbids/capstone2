let editCourse = document.querySelector("#editCourse")
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let adminUser = localStorage.getItem("token")
let userId = localStorage.getItem('id')

editCourse.addEventListener("submit", (e)=> {
	e.preventDefault()
	let courseName = document.querySelector("#courseName").value
	let coursePrice = document.querySelector("#coursePrice").value
	let courseDes= document.querySelector("#courseDescription").value

	if((courseName !== '' && courseDes !== '' && coursePrice !== '')){

	  	  	fetch(`https://booking-system-mabid.herokuapp.com/api/courses/`, {
		  	  	method: 'PUT',
		  	  	headers: {
		  	  		'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					_id : courseId,
					name : courseName,
					description : courseDes,
					price : coursePrice
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				
				//if updating is successful
				if(data){
					alert("Course updated Successfully")
					//redirect to courses
					window.location.replace("./courses.html")
				}else{
					//error occured in registration
					alert("Something Went wrong")
				}

			})
	  	}else{
	  		alert("Fill up the form")
		}
})
