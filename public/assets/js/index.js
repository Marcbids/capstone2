let navItems = document.querySelector('#navSession')
let profileLink = document.querySelector('#profileLink')

let userToken = localStorage.getItem("token")
let isAdmin = localStorage.getItem("isAdmin")

if(!userToken){
    navItems.innerHTML = `
        <li class="nav-item">
            <a  href="./pages/login.html" class="nav-link">Login<a/>
        </li>`
}else{
    navItems.innerHTML = `
        <li class="nav-item">
            <a  href="./pages/logout.html" class="nav-link">Logout<a/>
        </li>
        `
}

if(!userToken){
    profileLink.innerHTML = null
}
if(isAdmin == "true"){
    profileLink.innerHTML = null
}