let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter;
let container = document.querySelector("#coursesContainer")
let enrollees = document.querySelector("#enroleesContainer")

if(adminUser == "false" || !adminUser){
	//if the user is a regular user, do not show the add course button
	modalButton.innerHTML = null
}else{
	//display add course button if user is an admin
	modalButton.innerHTML =
	`
	<div style="display:flex;" class="offset-md-4 offset-lg-6">
	<div class="col-md-8">
		<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
	</div>
	<div class="col-md-8">
		<a href="./courses.html" class="btn btn-block btn-info" id="disabledCourses">View Disabled Courses</a>
	</div>
	</div>
	`
let disabledCourses = document.querySelector("#disabledCourses")

disabledCourses.addEventListener("click", (e) =>{
e.preventDefault()

fetch('https://booking-system-mabid.herokuapp.com/api/courses/disabled-courses')
.then(res => res.json())
.then(data => {
	
	modalButton.innerHTML =
	`
	<div style="display:block;" class="offset-md-10">
	<div class="col-md-12">
		<a href="./courses.html" class="btn btn-block btn-info" id="disabledCourses">Enabled Courses</a>
	</div>
	</div>
	`
	//log the data to check if you were able to fetch the data from our server
	let courseData;
	//if the number of courses fetched is less than 1, display no courses available
	if(data.length < 1){
		courseData = "<h2>No courses available."
		container.innerHTML = courseData
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
		//if the user is a regular user, display when the course was created and display the button, select course
		if(adminUser == "false" || !adminUser){
			cardFooter = 
			`
			<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block viewButton">Select Course
			</a>
			`
		}else{
		//else, if the user is admin, display the edit and delete button
		//when the edited button is clicked, it will redirect the user to editCourse.html(name of button=Edit/Update)
		//when the delete button is clicked, the course will be disabled(name of button=Disable Course)
		//key = courseId
		//value = the element's courseId
			cardFooter =
			 `
			<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Edit
			</a>
		
			<a href="./enable.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block enableButton"> Enable Course
			</a>

			<a href="./enrollees.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block view"> View Enrollees
			</a>
			
			`
		}
			return(
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								${course.price}
							</p>
						</div>
						<div class="card-footer">${cardFooter}
						</div>
					</div>	
				</div>
				`
			)
		}).join("")
		//since the collection is an array, we can use join to indicate the separator of each element to replace the comma
		container.innerHTML = courseData
	}
})

})
}


//fetch courses from API


fetch('https://booking-system-mabid.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	
	//log the data to check if you were able to fetch the data from our server
	let courseData;
	let enrolleesData;
	//if the number of courses fetched is less than 1, display no courses available
	if(data.length < 1){
		courseData = "<h2>No courses available."
		container.innerHTML = courseData
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {

		//if the user is a regular user, display when the course was created and display the button, select course
		if(adminUser == "false" || !adminUser){
			cardFooter = 
			`
			<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block viewButton">Select Course
			</a>
			`
		}else{

		//else, if the user is admin, display the edit and delete button
		//when the edited button is clicked, it will redirect the user to editCourse.html(name of button=Edit/Update)
		//when the delete button is clicked, the course will be disabled(name of button=Disable Course)
		//key = courseId
		//value = the element's courseId
			cardFooter = 
			`
			<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Edit
			</a>
		
			<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block deleteButton"> Disable Course
			</a>

			<a href="./enrollees.html?courseId=${course._id}" value={course._id} class="btn btn-info text-white btn-block view"> View Enrollees
			</a>


			`
		}

		return(
			`
			<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${course.name}</h5>
						<p class="card-text text-left">
							${course.description}
						</p>
						<p class="card-text text-right">
							${course.price}
						</p>
					</div>
					<div class="card-footer">${cardFooter}
					</div>
				</div>	
			</div>
			`
		)
		}).join("")
		//since the collection is an array, we can use join to indicate the separator of each element to replace the comma

		container.innerHTML = courseData
	}
})	




