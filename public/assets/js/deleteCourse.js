let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let courseDisabled = document.querySelector("#course")
let proceed = document.querySelector("#proceed")
let cancel = document.querySelector('#cancel')
let message = document.querySelector('#message')
let button = document.querySelector('#button')


courseDisabled.innerHTML = 
`
<span class="spinner-border text-primary"></span> Disabling the course...
<h4>Are you sure you want to disable this course?
`

	
proceed.addEventListener('click', (e)=>{

	fetch(`https://booking-system-mabid.herokuapp.com/api/courses/${courseId}`, {
		method: 'DELETE',
		headers: {
		  		'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			_id : courseId
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		// if(data.data.isActive == true){
		// 	courseDisabled.innerHTML = `<span class="spinner-border text-primary"></span> Disabling the course...
		// 								<h4>Are you sure you want to disable this course?`
		// }else{
		// 	courseDisabled.innerHTML = `<h2>Are you sure you want to enable this course?`
		// }
		if(data.data.isActive == false){
			courseDisabled.innerHTML = `<i class="bi bi-check"></i>Course with name ${data.data.name} has been disabled`
			button.innerHTML = `<a href="./courses.html" class="btn btn-outline-primary" id="cancel"> Go back to courses</a>`
		// }else{
		// 	courseDisabled.innerHTML = `<i class="bi bi-check"></i>Course with name ${data.data.name} has been Enabled`
		// 	button.innerHTML = `<a href="./courses.html" class="btn btn-outline-primary" id="cancel"> Go back to courses</a>`
		}
	})
})


// fetch(`http://localhost:3000/api/courses/enable-course/${courseId}`, {
// 	method: 'PUT',
// 	headers: {
// 	  		'Content-Type': 'application/json',
// 	},
// 	body: JSON.stringify({
// 		_id : courseId
// 	})
// })
// .then(res => {
// 	return res.json()
// })
// .then(data => {
// 	if(data.data.isActive == false){
// 		courseDisabled.innerHTML = `<i class="bi bi-check"></i>Course with name ${data.data.name} has been Enabled</h2>`
// 	}
// })