let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let enrolleesData = document.querySelector("#enrolleesData")
let tables = document.querySelector("#tables")
let token = localStorage.getItem("token")
let id = localStorage.getItem("id")


fetch(`https://booking-system-mabid.herokuapp.com/api/courses/${courseId}`,  {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
	}
})
.then(res => res.json())
.then(data => {
let userData;

enrolleesData.innerHTML = `Enrollees for course ${data.name}`

let tables = document.querySelector("#tableBody")
		
	userData = data.Enrollees.map(result => {
				return(
					`	
					    <tr>
					      <th scope="row" id="courseName">${result.fullname}</th>
					      <td>${result.enrolledOn}</td>
					      <td>${result.userId}</td>
					    </tr>
					
					`
				)
		
	}).join("")
tables.innerHTML = userData
})