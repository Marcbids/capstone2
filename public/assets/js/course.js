let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let adminUser = localStorage.getItem("token")
let description = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollContainer")
let name = document.querySelector("#courseName")
let userId = localStorage.getItem('id')
let fullname = localStorage.getItem("fullname")


fetch(`https://booking-system-mabid.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then((data) => {
	
	// function isIndex(isId){
	// 		return isId.userId === localStorage.getItem('id')
	// 	}
	// 	console.log(localStorage.getItem('id') == data.Enrollees)
	if(data){
		name.innerHTML = data.name
		description.innerHTML = data.description
		price.innerHTML = data.price
		// if(localStorage.getItem('id') !== data.Enrollees[data.Enrollees.findIndex(isIndex)].userId){
		// let task = data.Enrollees.user(localStorage.getItem('id'))
		// console.log(task)
		if(!userId){
			enrollContainer.innerHTML = 
				`<button href="#" value={data._id} class="=btn btn-primary text-white btn-block viewButton" id="registerFirst">Login First to Enroll
				</button>
				<button href="#" value={data._id} class="" id="enroll" style="display:none;">Enroll
				</button>`
		}else{
			if(data.Enrollees.length !== 0){
				for (let i = 0 ; i <data.Enrollees.length; i++) {
					if (data.Enrollees[i].userId == userId) {
						enrollContainer.innerHTML = 
						`<button href="#" value={data._id} class="disabled btn btn-primary text-white btn-block viewButton" id="alreadyEnrolled" >Already Enrolled
						</button>
						<button href="#" value={data._id} class="=btn btn-primary text-white btn-block viewButton" id="registerFirst" style="display:none;">Register First to Enroll
						</button>
						<button href="#" value={data._id} class="" id="enroll" style="display:none;">Enroll
						</button>`
						break;
					}else{
						enrollContainer.innerHTML = 
						`<button href="#" value={data._id} class="btn btn-primary text-white btn-block viewButton" id="enroll" >Enroll
						</button>
						<button href="#" value={data._id} class="=btn btn-primary text-white btn-block viewButton" id="registerFirst" style="display:none;">Register First to Enroll
						</button>`
					}
				}
			}else{
				enrollContainer.innerHTML = 
				`<button href="#" value={data._id} class="btn btn-primary text-white btn-block viewButton" id="enroll" >Enroll
				</button>
				<button href="#" value={data._id} class="=btn btn-primary text-white btn-block viewButton" id="registerFirst" style="display:none;">Register First to Enroll
				</button>`
			}
		}
		// }else{
			// enrollContainer.innerHTML = 
			// `<button href="#" value={data._id} class="btn btn-secondary text-white btn-block viewButton" id="enroll">Already Enrolled
			// 		</button>`
		// }
	let register = document.querySelector("#registerFirst")

	registerFirst.addEventListener("click", (e) => {
		window.location.replace("./register.html")
	})

	let enrollBtn = document.querySelector("#enroll")

	enrollBtn.addEventListener("click", (e) => {
	  	e.preventDefault()

		fetch('https://booking-system-mabid.herokuapp.com/api/user/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${adminUser}`
			},
			body: JSON.stringify({
				"courseId" : courseId,
				"courseName": data.name,
				"fullname": fullname
			})
		})
		.then(res => {
			return res.json()
		})
		.then(result => {
				alert("Successfully Enrolled!")
				window.location.reload()
			
		})
	})


	}else{
		alert("Data unavailable")
	}
	
})

